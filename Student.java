public class Student{
	
	private String name;
	private String studyField;
	private double unpaidFees;
	private int amountLearnt;
	
	//constructor
	
	public Student(String name, double unpaidFees){
		
		//name = "Unknown";
		this.name = name;
		this.studyField = "Undefined";
		//this.unpaidFees = -280;
		this.unpaidFees = unpaidFees;
		this.amountLearnt = 0;
	}
	
	//setter
	public void setName(String name){
		this.name = name;
	}
	
	public void setStudyField(String studyField){
		this.studyField = studyField;
	}
	
	public void setUnpaidFees(double unpaidFees){
		this.unpaidFees = unpaidFees;
	}
	
	//getter
	public String getName(){
		return this.name;
	}
	
	public String getStudyField(){
		return this.studyField;
	}
	
	public double getUnpaidFees(){
		return this.unpaidFees;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	// instance methods
	
	public void introduceSelf(){
		System.out.println("\nHi guys, I'm " + name + "\nand I study in " + studyField + " at Dawson College");
	}
	
	public void repayFees(double repayAmount){
		System.out.println("\nWant to Repay your Fees? \nBalance: "+ (-unpaidFees));
		System.out.println("You want to repay " + repayAmount + " $?");
		
		unpaidFees = unpaidFees - repayAmount;
		System.out.println("\nDone! Here your new balance... \nBalance: "+ (-unpaidFees));
	}
	
	public void study(int amountStudied){
		this.amountLearnt += amountStudied;
	}
}