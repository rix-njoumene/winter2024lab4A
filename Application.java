import java.util.Scanner;

public class Application{
	
	public static void main (String[] args){
		
		//array of Students
		Student[] section3 = new Student[4];
		
		for (int i = 0; i < section3.length; i++){
			section3[i] = new Student("Mystery", 100);
			//System.out.println( i + " created");
		}
		
		//new students
		
		Student Rix = new Student("Ryan Nj", 40);
		System.out.println("Name?:" + Rix.getName());
			//Rix.setName("Rix Njoumene");
		Rix.setStudyField("Computer Science");
			//Rix.setUnpaidFees(50.00);
		
		
		Student Mars = new Student("Planet", 110);
			//Mars.setName("Mars ThePlanet");
		Mars.setStudyField("Pure & Applied Science");
			//Mars.setUnpaidFees(120.00);
		
		System.out.println("Student 0 Name: " + Rix.getName() );
		System.out.println("Student 0 Name: " + Mars.getName() );
		
		
		Rix.introduceSelf();
			//Mars.setUnpaidFees(Mars.getUnpaidFees() -30.00);
		
		//array section3
		section3[0] = Rix;
		section3[1] = Mars;
		
		
			//section3[2].setName("Third TheWheel");
		section3[2].setStudyField("Law & Justice");
			//section3[2].setUnpaidFees(75.00);
			
		/*System.out.println(section3[0].unpaidFees);
		System.out.println(section3[2].name);*/
		
		
		section3[3].setStudyField("Social Science");
		System.out.println(section3[3].getName() + "\n" + section3[3].getStudyField());
		System.out.println(section3[3].getUnpaidFees() + "\n" + section3[3].getAmountLearnt());
		
		Scanner reader = new Scanner(System.in);
		

		
		for (int i = 0; i<2; i++){
			System.out.println("\nEnter the amount of time that " +section3[2].getName() + " has studied" );
			int amountStudied = reader.nextInt();
			section3[2].study(amountStudied);
		}
		
		
		System.out.println("\nAmount learnt by all student");
		for (int i = 0; i<section3.length; i++){
			System.out.println(section3[i].getName() +": "+ section3[i].getAmountLearnt());
		}
		
	}
}